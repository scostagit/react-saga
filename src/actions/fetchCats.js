const fetchCatsError = () => {
	return { type: 'FETCH_CATS_ERROR' };
};

const fetchCatsStarted = () => {
	return { type: 'FETCH_CATS_START' };
};

const fetchCatsSuccess = (cat) => {
	return { type: 'FETCH_CATS_SUCCESS', payload: cat };
};

// fetching a random cat starts now
const fetchCats = () => async (dispatch) => {
	dispatch(fetchCatsStarted());
	try {
		const catResponse = await fetch('https://api.thecatapi.com/v1/images/search', {
			headers: {
				'Content-Type': 'application/json',
				'x-api-key': 'YOUR_API_KEY'
			}
		});

		const cat = await catResponse.json();
		dispatch(fetchCatsSuccess(cat));
	} catch (exc) {
		dispatch(fetchCatsError());
	}
};

export default {
	fetchCats
};
