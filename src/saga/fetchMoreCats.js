//./saga/fetchMoreCats

import { takeLatest, put } from 'redux-saga/effects';

//Every time we dispatch an action
//that has a type property "FETCH_MORE_CATS"
// call the fetchMoreCatsSaga function
export default function* watchFetchMoreCatsSaga() {
	yield takeLatest('FETCH_MORE_CATS', fetchMoreCatsSaga);
}

//query 5 cat image at the same time
function* fetchMoreCatsSaga() {
	yield put({ type: 'FETCH_MORE_CATS_SAGA_START' });

	const catResponse = yield fetch('https://api.thecatapi.com/v1/images/search?limit=5', {
		headers: {
			'Content-Type': 'application/json',
			'x-api-key': 'YOUR_API_KEY'
		}
	});

	const cats = yield catResponse.json();

	yield put({ type: 'FETCH_MORE_CATS_SAGA_SUCCESS', payload: cats });
}
