import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers/index';
import createSagaMiddleware from 'redux-saga';
import watchFetchMoreCatsSaga from './saga/fetchMoreCats';
import App from './App';

//init
const sagaMiddleware = createSagaMiddleware();

const loggerMiddleware = createLogger();

const store = createStore(rootReducer, applyMiddleware(thunkMiddleware, sagaMiddleware, loggerMiddleware));

//we need to run and pass a saga as parameter
sagaMiddleware.run(watchFetchMoreCatsSaga);

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root')
);
